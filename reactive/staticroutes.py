from collections import defaultdict

import csv
import hashlib
import subprocess
import os

from io import StringIO

from charms.reactive import when, when_any
from charms.templating.jinja2 import render

from charmhelpers.core import hookenv
from charmhelpers.core.hookenv import status_set

from charmhelpers.contrib.network import ip

INTERFACES_D = '/etc/network/interfaces.d/{0}.cfg'
ROUTE_CMD = 'ip {0} route {1} {2} via {3} dev {4}'

def parse_config(config):
    s = StringIO(config)
    reader = csv.DictReader(s, fieldnames=('network', 'nexthop', 'route'))
    all_routes = []

    for r in reader:
        all_routes.append({
            'network': r['network'].strip(),
            'nexthop': r['nexthop'].strip(),
            'route': r['route'].strip(),
        })

    return all_routes

def hash_route(network, nexthop, route):
    h = hashlib.sha256()
    h.update(network.encode('utf-8'))
    h.update(nexthop.encode('utf-8'))
    h.update(route.encode('utf-8'))
    return h.hexdigest()

def calculate_diff(tostate, fromstate):
    existing = []
    for r in fromstate:
        existing.append(hash_route(r['network'], r['nexthop'], r['route']))

    diff = []
    for r in tostate:
        h = hash_route(r['network'], r['nexthop'], r['route'])

        if h not in existing:
            diff.append(r)

    return diff

def calculate_added(current, previous):
    return calculate_diff(current, previous)

def calculate_removed(current, previous):
    return calculate_diff(previous, current)

def format_add_route(interface, nexthop, route):
    route_add = ROUTE_CMD.format(
        '-6' if ip.is_ipv6(nexthop) else '',
        'add',
        route,
        nexthop,
        interface)
    return route_add

def add_route(interface, nexthop, route):
    route_add = format_add_route(interface, nexthop, route)

    hookenv.log('Adding route: {}'.format(route_add))

    try:
        subprocess.check_call(route_add.split())
    except subprocess.CalledProcessError as e:
        if e.returncode == 2:
            return

        hookenv.log(
            'Failed to add route to interface <{0}>: {1} via {2}'.format(
                interface,
                route,
                nexthop
                )
        )

def remove_route(interface, nexthop, route):
    route_del = ROUTE_CMD.format(
        '-6' if ip.is_ipv6(nexthop) else '',
        'del',
        route,
        nexthop,
        interface)

    try:
        subprocess.check_call(route_del.split())
    except subprocess.CalledProcessError as e:
        if e.returncode == 2:
            return

        hookenv.log(
            'Failed to remove route from interface <{0}>: {1} via {2}'.format(
                interface,
                route,
                nexthop
                )
        )

def get_interface_for_network(net):
    address = ip.get_address_in_network(net, fallback=None)

    if address:
        return ip.get_iface_from_addr(address)

@when('config.changed')
def install_routes():
    config = hookenv.config()

    changed = config.changed('routes')
    current = parse_config(config['routes'])

    status_set('maintenance', 'Route configuration has changed.')

    if changed:
        status_set('maintenance', 'Updating routes.')
        hookenv.log("Config has changed from it's previous value!")
        previous = parse_config(config.previous('routes'))
        removed = calculate_removed(current, previous)
        added = calculate_added(current, previous)

        for r in removed:
            interface = get_interface_for_network(r['network'])

            # Looks like we don't have an interface on that network
            if interface is None:
                continue

            # Delete rendered interfaces file for this interface
            os.remove(INTERFACES_D.format(interface))

            # Pull the route out of the routing table
            remove_route(interface, r['nexthop'], r['route'])

        for r in added:
            interface = get_interface_for_network(r['network'])

            # We don't have an interface on this new network, not adding a route
            if interface is None:
                continue

            # Install the new route in the routing table
            add_route(interface, r['nexthop'], r['route'])

    interface_routes = defaultdict(list)

    # Find out what interfaces the routes are associated with.
    for r in current:
        interface = get_interface_for_network(r['network'])

        if interface is not None:
            interface_routes[interface].append(r)

            # If a change has happened, this will already have been completed,
            # so let's not do it again.
            if not changed:
                add_route(interface, r['nexthop'], r['route'])

    if len(interface_routes.keys()) == 0:
        status_set('active', 'There are no routes to install.')
        hookenv.log('There is nothing to do.')
        return

    # Go over our list of interfaces, and render iface files
    for (interface, routes) in interface_routes.items():
        path = INTERFACES_D.format(interface)
        hookenv.log('Rendering iface to: {}'.format(path))

        render(
            'iface',
            path,
            {
                'interface': interface,
                'routes': [
                    format_add_route(
                        interface,
                        r['nexthop'],
                        r['route']
                    ) for r in routes
                ]
            },
            owner='root',
            group='root'
        )

    status_set('active', 'Routes have been successfully installed.')

@when_any('host-system.available', 'host-system.connected')
def host_available():
    pass
