JuJu staticroutes Charm
-----------------------

Currently there is no way for staticroutes to be added to interfaces that may
require it, such as on servers with multiple interfaces, or where specific
traffic has to be routed over particular interfaces.

This charm helps add those static routes as necessary.

Usage
~~~~~

How to use this charm:

.. code::

   juju deploy cs:~bertjwregeer/staticroutes
   juju add-relationship othercharm staticroutes

This makes staticroutes a sub-ordinate of the charm mentioned. This allows it
to easily be deployed across systems in a cluster.

Configuration needs to be provided for this Charm to have any effect, if no
configuration is provided, this Charm is a no-op.

Configuration
~~~~~~~~~~~~~

Nothing special yet.

Known Limitations and Issues
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

None at this moment.

Contact Information
~~~~~~~~~~~~~~~~~~~

This charm was created by Bert JW Regeer <bertjw@regeer.org>

 - Project: https://gitlab.com/bertjwregeer/juju_staticroutes/
 - Issues: https://gitlab.com/bertjwregeer/juju_staticroutes/issues
